import { createContext, useContext, useState } from "react";

const ScreenBlurContext = createContext({
  showBlur: false,
  setShowBlur: (value) => {},
});

const ScreenBlurProvider = ({ children }) => {
  const [showBlur, setShowBlur] = useState(false);

  return (
    <ScreenBlurContext.Provider value={{ showBlur, setShowBlur }}>
      {children}
    </ScreenBlurContext.Provider>
  );
};

export default ScreenBlurProvider;

export const useScreenBlur = () => {
  return useContext(ScreenBlurContext);
};
