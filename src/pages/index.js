import Layout from "@components/Layout";
import gsap from "gsap";
import Head from "next/head";
import Image from "next/image";
import Link from "next/link";
import { useEffect, useRef, useState } from "react";
import { useIntersection } from "react-use";

const heroImages = [
  "red-work-flip-thumb.jpg",
  "red-work-snickers-thumb.jpg",
  "red-work-thumbs-cars.jpg",
  "red-case-kovitz-thumb.jpg",
  "red-sports-thumbs.jpg",
  "red-icebox_cover-thumb.jpg",
  "red-work-hardrock-thumbs.jpg",
  "red-work-thenorth-thumb.jpg",
  "red-bad-testing-thumb.jpg",
  "red-work-soco-thumb.jpg",
  "red-work-wafflestop-Hero.jpg",
  "red-work-reeses-thumb.jpg",
];

const clientListOne = [
  "Cherokee Casinos",
  "Flipboard",
  "Foxwoods",
  "Glanbia Nutritionals",
  "Google",
  "Hard Rock",
  "Hibbett Sports",
  "Hilton Worldwide",
  "Jack Daniel’s",
];
const clientListTwo = [
  "Nescafé",
  "New York Pride",
  "Patreon",
  "Rivers Casino",
  "Snickers",
  "Southern Comfort",
  "The University of Alabama",
  "Twix",
  "Wind Creek Hospitality",
];

export default function Home() {
  const [isActive, setIsActive] = useState(0);
  const sectionRef = useRef(null);
  const intersection = useIntersection(sectionRef, {
    root: null,
    rootMargin: "0px",
    threshold: 1,
  });

  const fadeIn = (element) => {
    // console.log("fade.in");
    gsap.to(element, {
      opacity: 1,
      duration: 1,
      // y: 0,
      ease: "power4.out",
    });
  };

  const fadeOut = (element) => {
    // console.log("fade.out");
    gsap.to(element, {
      opacity: 0,
      duration: 1,
      ease: "power4.out",
    });
  };

  useEffect(() => {
    const imageInterval = setInterval(() => {
      setIsActive((prevVal) => {
        if (prevVal === heroImages.length - 1) {
          setIsActive(0);
          return;
        }
        setIsActive(prevVal + 1);
        return;
      });
    }, 250);

    return () => {
      clearInterval(imageInterval);
    };
  }, []);

  useEffect(() => {
    if (!intersection) return;
    intersection.intersectionRatio < 0.2
      ? fadeOut(".fadeIn")
      : fadeIn(".fadeIn");
    return () => {};
  }, [intersection]);

  return (
    <Layout>
      <main className="home">
        <section className="hero">
          <div className="hero-top">
            <h1 className="hero__title">The agency for what comes next.</h1>
          </div>
          <div className="hero-bottom">
            <Link href="/work">
              <a>
                <div className="hero__image">
                  {heroImages.map((image, i) => {
                    // if (isActive !== i) return;
                    return (
                      <Image
                        key={image}
                        src={`/images/${image}`}
                        className={`hero__image__item ${
                          isActive === i ? "active" : ""
                        }`}
                        alt="Hero Image"
                        layout="fill"
                        priority
                        // placeholder="blur"
                        // blurDataURL={`/images/${image}`}
                      />
                    );
                  })}
                </div>
              </a>
            </Link>
          </div>
        </section>
        <section className="ideas" ref={sectionRef}>
          <p className="our-deal fadeIn">
            We deal in ideas, design and media that are category defying. When
            the world Millis, you’ve got to Vanilli.
          </p>
          <p className=" fadeIn">
            Red Square believes in rigorous development of brand strategy and
            whip-smart execution. We make all sorts of things. Things that move
            markets, compel audiences and sell product. We help great brands
            create what comes next.
          </p>
        </section>

        <div className="separator" style={{ marginTop: 0, marginBottom: 0 }}>
          <hr />
        </div>

        <section className="clients">
          <div className="client-list-wrapper">
            <div>
              <h3 className="text-muted">Select clients</h3>
            </div>

            <ul>
              {clientListOne.map((client) => {
                return <li key={client}>{client}</li>;
              })}
            </ul>
            <ul>
              {clientListTwo.map((client) => {
                return <li key={client}>{client}</li>;
              })}
            </ul>
          </div>
        </section>

        <div className="separator" style={{ marginTop: 0, marginBottom: 0 }}>
          <hr />
        </div>
      </main>
    </Layout>
  );
}
