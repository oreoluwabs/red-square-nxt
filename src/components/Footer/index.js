import Link from "next/link";
import { SiLinkedin, SiTwitter } from "react-icons/si";
import { AiFillPlusCircle, AiFillInstagram } from "react-icons/ai";
import { useScreenBlur } from "@hooks/useScreenBlur";
import { useCallback, useEffect, useRef } from "react";

const counter = 0;
const updateRate = 10;
const isTimeToUpdate = function () {
  return counter++ % updateRate === 0;
};

const Footer = () => {
  const { setShowBlur } = useScreenBlur();
  const footerContact = useRef(null);
  const mouseOrigin = useRef({
    _x: 0,
    _y: 0,
    x: 0,
    y: 0,
    updatePosition: function (event) {
      const e = event || window.event;
      this.x = e.clientX - this._x;
      this.y = (e.clientY - this._y) * -1;
    },
    setOrigin: function (e) {
      this._x = e.offsetLeft + Math.floor(e.offsetWidth / 2);
      this._y = e.offsetTop + Math.floor(e.offsetHeight / 2);
    },
    show: function () {
      return "(" + this.x + ", " + this.y + ")";
    },
  });

  useEffect(() => {
    if (!footerContact?.current) return;
    mouseOrigin.current.setOrigin(footerContact.current);
  }, []);

  const updateMouse = (ev) => {
    const inner = footerContact.current.children[0];
    mouseOrigin.current.x = ev.clientX - mouseOrigin.current._x;
    mouseOrigin.current.y = ev.clientY - mouseOrigin.current._y * -1;

    updateTransformStyle(
      (mouseOrigin.current.y / inner.offsetHeight / 2).toFixed(2),
      (mouseOrigin.current.x / inner.offsetWidth / 2).toFixed(2)
    );
  };

  const updateTransformStyle = (x, y) => {
    const inner = footerContact.current.children[0];
    const style = "rotateX(" + x + "deg) rotateY(" + y + "deg)";

    inner.style.transform = style;
    inner.style.webkitTransform = style;
    inner.style.mozTransform = style;
    inner.style.msTransform = style;
    inner.style.oTransform = style;
  };

  return (
    <footer className="footer container">
      <div className="footer__content">
        <div
          ref={footerContact}
          className="footer__contact"
          onMouseMove={(ev) => {
            updateMouse(ev);
          }}
          onMouseLeave={() => {
            setShowBlur(false);

            const inner = footerContact.current.children[0];
            const style = "";
            inner.style = style;
          }}
          onMouseEnter={(ev) => {
            setShowBlur(true);
            if (isTimeToUpdate()) {
              updateMouse(ev);
            }
          }}
        >
          <Link href="mailto:hi@rsq.com">
            <a>
              <h4>
                Let’s talk. <span className="text-primary">hi@rsq.com</span>
              </h4>
            </a>
          </Link>
        </div>
        <div className="social-location-wrapper">
          <div className="social-icons">
            <ul>
              <li>
                <button className="nugs__button">
                  <AiFillPlusCircle />
                  <span>Nugs</span>
                </button>
              </li>
              <li>
                <Link href="/">
                  <a>
                    <AiFillInstagram />
                  </a>
                </Link>
              </li>
              <li>
                <Link href="/">
                  <a>
                    <SiTwitter />
                  </a>
                </Link>
              </li>
              <li>
                <Link href="/">
                  <a>
                    <SiLinkedin />
                  </a>
                </Link>
              </li>
            </ul>
          </div>
          <div className="locations">
            <h4 className="text-muted">
              <ul>
                <li>Mobile, AL</li>
                <li>Chicago, IL</li>
                <li>Tulsa, OK</li>
              </ul>
            </h4>
          </div>
        </div>
      </div>
    </footer>
  );
};

export default Footer;
