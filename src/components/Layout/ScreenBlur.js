import { useScreenBlur } from "@hooks/useScreenBlur";

const ScreenBlur = () => {
  const { showBlur } = useScreenBlur();

  //   return showBlur ? (
  //     <div className="screen-blur" data-is-showing={showBlur} />
  //   ) : null;
  return <div className="screen-blur" data-is-showing={showBlur} />;
};

export default ScreenBlur;
