import { useScreenBlur } from "@hooks/useScreenBlur";
import Link from "next/link";
import { CgChevronRight } from "react-icons/cg";

const Header = () => {
  const { setShowBlur } = useScreenBlur();
  return (
    <header className="header">
      <nav>
        <div>
          <Link href="/">
            <a className="logo" aria-label="Red Square" />
          </Link>
        </div>
        <div>
          <Link href="/">
            <a
              className="nav-link"
              onMouseLeave={() => setShowBlur(false)}
              onMouseEnter={() => setShowBlur(true)}
            >
              Work
              <CgChevronRight className="link-icon" />
            </a>
          </Link>
        </div>
      </nav>
    </header>
  );
};

export default Header;
